<?php

/**
 * @file
 * Contains the Classic Catalog Search Form.
 */

/**
 * Generate the WebPAC Search Widget form.
 */
function mysite_classic_catalog_form($form, $form_state) {
  $form = [];
  $form['#action'] = 'https://clara.hfcc.edu/search/X';
  $form['#method'] = 'get';
  $form['searchtype'] = [
    '#type' => 'radios',
    '#title' => t('Search Type'),
    '#options' => [
      'X' => t('Keyword'),
      't' => t('Title'),
      'a' => t('Author'),
      'd' => t('Subject'),
    ],
    '#default_value' => 'X',
    '#description' => t("Select the type of search to perform."),
  ];
  $form['searcharg'] = [
    '#type' => 'textfield',
    '#title' => t('Search terms'),
    '#size' => 25,
    '#maxlength' => 75,
    '#placeholder' => 'enter search terms',
    '#description' => t("Type the search term you want to find. Examples: <ul><li>Henry Ford and Benjamin B. Lovett</li><li>Henry Ford</li></ul>"),
  ];
  $form['SORT'] = [
    '#type' => 'hidden',
    '#value' => 'D',
  ];
  $form['submit'] = [
    '#button_type' => 'submit',
    '#name' => 'Submit',
    '#value' => 'Search',
    '#type' => 'submit',
  ];
  $linklist = [
    l(t('Search Course Reserves'), 'http://clara.hfcc.edu/search/r'),
    l(t('Advanced Search'), 'http://clara.hfcc.edu/search/X'),
  ];
  $form[] = [
    '#theme' => 'item_list',
    '#items' => $linklist,
    '#attributes' => ['class' => 'advanced-links inline'],
  ];
  return $form;
}
